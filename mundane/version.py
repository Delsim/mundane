"""App version string.

    (c) Gibbs Consulting, a division of 0802100 (BC) Ltd, 2021

"""


__version__ = "0.0.1"
