#!/usr/bin/env python

from setuptools import setup, find_packages

with open('mundane/version.py') as f:
    exec(f.read())

with open('README.md') as f:
    long_description = f.read()

setup(
    name="mundane",
    version=__version__,
    url="https://gitlab.com/GibbsConsulting/mundane",
    description="Mundane temporality for time series objects",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Gibbs Consulting",
    author_email="mundane@gibbsconsulting.ca",
    license='AGPLv3',
    packages=find_packages(),
    classifiers = [
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: GNU Affero General Public License v3',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.7',
    ],
    install_requires = ['pandas',
                        ],
    python_requires=">=3.7",
    include_package_data = True,
    zip_safe = False,
    )

