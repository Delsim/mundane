FROM python:3.7
RUN mkdir -p /project && mkdir -p /project/scripts
COPY scripts/requirements.txt /project/scripts/
RUN pip install --upgrade pip && pip install -r /project/scripts/requirements.txt
